# Evernote Bookmark Importer

While migrating all my stuff from springpad to evernote, I created a quick and dirty perl script.
Call the script and give it a .html file with links as argument:
 
_perl genex_bookmarks.pl mybookmarks.html_

## Info

Works great with bookmarks files, eg. export bookmarks as .html from firefox or chrome.
Basically works just with every other .html file. It just parses all the a-href tags.
It may even work with exports from pocket, readability etc... (untested).
 
The script tags all found links as ‘bookmark’ and writes an .enex file, which can be imported with the desktop version (win / mac / whatever) of evernote.

## Requirements:
* perl


__Have Fun!__